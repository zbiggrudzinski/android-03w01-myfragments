package com.example.kjankiewicz.android_03w01;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/*
 * Fragment rozszerzający klasę ListFragment
 * Różne typy fragmentów
 * Android-03w-interfejs-cz1: slajd 13
 */
public class MyContinentsFragment extends ListFragment {
    private static final String TAG = "ContinentsFragmentTag";
    private ContinentSelectionListener mListener = null;

    public static String[] mContinentsArray;
    ArrayAdapter<String> mContinentsArrayAdapter;

    public interface ContinentSelectionListener {
        public void onListSelection(int index);
    }

    @Override
    public void onListItemClick(ListView l, View v, int pos, long id) {
        getListView().setItemChecked(pos, true);
        mListener.onListSelection(pos);
    }

    @Override
    public void onAttach(Activity activity) {
        Log.i(TAG, "onAttach()");
        super.onAttach(activity);

        try {
            mListener = (ContinentSelectionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ContinentSelectionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate()");
        setRetainInstance(true);
        mContinentsArray = getResources().getStringArray(R.array.Continents);
        mContinentsArrayAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.continent_item, mContinentsArray);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView()");
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    /*
     * Cykl życia fragmentu
     * Metoda wywoływana, gdy aktywność zawierająca
     * fragment zakończyła realizację onCreate
     * Android-03w-interfejs-cz1: slajdy 5 i 6
     */
    @Override
    public void onActivityCreated(Bundle savedState) {
        Log.i(TAG, "onActivityCreated()");
        super.onActivityCreated(savedState);
        setListAdapter(mContinentsArrayAdapter);
        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop()");
        super.onStop();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, "onDetach()");
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "onDestroyView()");
        super.onDestroyView();
    }

}
