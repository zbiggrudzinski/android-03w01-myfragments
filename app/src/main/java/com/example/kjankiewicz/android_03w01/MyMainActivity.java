package com.example.kjankiewicz.android_03w01;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/*
 * Aktywność implementuje odbiornik ContinentSelectionListener
 * Komunikacja pomiędzy fragmentem a aktywnością
 * Android-03w-interfejs-cz1: slajd 10
 */
public class MyMainActivity extends Activity
        implements MyContinentsFragment.ContinentSelectionListener {

    private static final String TAG = "MainActivityTag";

    private static final String CURRENT_INDEX = "CURRENT_INDEX";

    private MyCitiesFragment mCitiesFragment;
    private MyContinentsFragment mContinentsFragment;
    private FragmentManager mFragmentManager;
    private FrameLayout mContinentsFrameLayout, mCitiesFrameLayout;

    private int currentIndex = -1;

    private static final int MATCH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "onCreate()");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_main);

        mContinentsFrameLayout = (FrameLayout) findViewById(R.id.continents_fragment_container);
        mCitiesFrameLayout = (FrameLayout) findViewById(R.id.cities_fragment_container);

        mFragmentManager = getFragmentManager();

        mFragmentManager
                .addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {
                        setLayout();
                    }
                });

        if (savedInstanceState == null) {
            /*
             * Dynamiczne dołączanie fragmentów do aktywności
             * Android-03w-interfejs-cz1: slajd 8 i 9
             */
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            mContinentsFragment = new MyContinentsFragment();
            fragmentTransaction.add(R.id.continents_fragment_container, mContinentsFragment);
            fragmentTransaction.commit();
        } else {
            currentIndex = savedInstanceState.getInt(CURRENT_INDEX);
            if (currentIndex >= 0) onListSelection(currentIndex);
        }
    }

    private void setLayout() {
        if (!mCitiesFragment.isAdded()) {
            mContinentsFrameLayout
                    .setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
            mCitiesFrameLayout.setLayoutParams(
                    new LinearLayout.LayoutParams(0, MATCH_PARENT));
        } else {
            mContinentsFrameLayout
                    .setLayoutParams(new LinearLayout.LayoutParams(0, MATCH_PARENT, 1f));
            mCitiesFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(0, MATCH_PARENT, 2f));
        }
    }

    /*
     * Implementacja metody z interfejsu odbiornika ContinentSelectionListener
     * Komunikacja pomiędzy fragmentem a aktywnością
     * Android-03w-interfejs-cz1: slajd 10
     */
    @Override
    public void onListSelection(int index) {

        currentIndex = index;

        if (mCitiesFragment == null) {
            mCitiesFragment = new MyCitiesFragment();
        }

        if (!mCitiesFragment.isAdded()) {
            /*
             * Dynamiczne dołączanie fragmentów do aktywności
             * Android-03w-interfejs-cz1: slajd 8 i 9
             */
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.cities_fragment_container, mCitiesFragment);
            fragmentTransaction.addToBackStack(null);

            fragmentTransaction.commit();
            mFragmentManager.executePendingTransactions();
        }
        mCitiesFragment.showIndex(index);
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause()");
        if (mCitiesFragment != null) {
            if (mCitiesFragment.isAdded()) {
                /*
                 * Dynamiczne dołączanie fragmentów do aktywności
                 * Android-03w-interfejs-cz1: slajd 8 i 9
                 */
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                /*
                 * Zdejmowanie fragmentów ze stosu wywołań
                 * Android-03w-interfejs-cz1: slajd 11
                 */
                mFragmentManager.popBackStack();
                fragmentTransaction.remove(mCitiesFragment);
                fragmentTransaction.commit();
                mFragmentManager.executePendingTransactions();
            } else currentIndex = -1;
        } else currentIndex = -1;

        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(CURRENT_INDEX, currentIndex);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestart() {
        Log.i(TAG, "onRestart()");
        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart()");
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop()");
        super.onStop();
    }

}